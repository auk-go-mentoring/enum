package jsontype

// var allTypesX = []string{
// 	"Invalid",
// 	"OptionA",
// 	"OptionB",
// 	"OptionC",
// 	"OptionD",
// }

var (
	allTypes = [...]string{
		Invalid:   "Invalid",
		OptionA:   "OptionA",
		OptionB:   "OptionB",
		Something: "Something",
		OptionC:   "OptionC",
		OptionD:   "OptionD",
	}
)
