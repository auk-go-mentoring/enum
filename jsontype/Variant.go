package jsontype

// Variant
//
// Reference:
//   - https://drive.google.com/file/d/1zSOTWixLOx0XqVeTVW0qaZPIXujQNTVs/view?usp=drive_link
//   - https://www.sohamkamani.com/golang/enums
//   - https://gitlab.com/auk-go/enum/-/blob/main/dbaction/Variant.go?ref_type=heads
//   - https://stackoverflow.com/a/35631797
//   - https://stackoverflow.com/a/17989915
//   - https://stackoverflow.com/a/14426447
//   - https://stackoverflow.com/a/71279479
type Variant byte

const (
	Invalid Variant = iota
	OptionA
	OptionB
	Something
	OptionC
	OptionD
	OptionE
)

func (it Variant) IsInvalid() bool {
	return it == Invalid
}

func (it Variant) IsOptionA() bool {
	return it == OptionA
}

func (it Variant) IsDetect() bool {
	return it == OptionA
}

func (it Variant) String() string {
	return allTypes[it]
}
