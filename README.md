# enum

## Getting started

Enum training repo by Md. Alim Ul Karim.

## References

- Training Video : https://drive.google.com/file/d/1zSOTWixLOx0XqVeTVW0qaZPIXujQNTVs/view?usp=drive_link
- https://www.sohamkamani.com/golang/enums
- https://gitlab.com/auk-go/enum/-/blob/main/dbaction/Variant.go?ref_type=heads
- https://stackoverflow.com/a/35631797
- https://stackoverflow.com/a/17989915
- https://stackoverflow.com/a/14426447
- https://stackoverflow.com/a/71279479

## Golang Enum Generator

GitHub - dmarkham/enumer: A Go tool to auto generate methods for your enums
https://github.com/dmarkham/enumer

stringer command - golang.org/x/tools/cmd/stringer - Go Packages
https://pkg.go.dev/golang.org/x/tools/cmd/stringer?utm_source=godoc

```go
type Pill int

const (
    Placebo Pill = iota
    Aspirin
    Ibuprofen
    Paracetamol
    Acetaminophen = Paracetamol
)

func (i Pill) String() string {
	//...
}

func PillString(s string) (Pill, error) {
	//...
}

func PillValues() []Pill {
	//...
}

func PillStrings() []string {
	//...
}

func (i Pill) IsAPill() bool {
	//...
}

func (i Pill) MarshalJSON() ([]byte, error) {
	//...
}

func (i *Pill) UnmarshalJSON(data []byte) error {
	//...
}

```
