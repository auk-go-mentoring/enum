module gitlab.com/auk-go-mentoring/enum

go 1.21.3

require (
	gitlab.com/auk-go/core v1.4.4
	gitlab.com/auk-go/enum v0.5.4
)
