package enum

import "fmt"

type jsonGenerateOptionType struct {
	OptionA int
	OptionB int
}

var (
	JsonGenerateOptionType = jsonGenerateOptionType{
		OptionA: 1,
		OptionB: 2,
	}
)

func main() {
	fmt.Println("hello")
	jsonGenerateOption := 1

	if jsonGenerateOption == JsonGenerateOptionType.OptionA {

	}

	switch jsonGenerateOption {
	case JsonGenerateOptionType.OptionA:
	}
}
