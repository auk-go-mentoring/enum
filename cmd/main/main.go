package main

import (
	"fmt"
	"unsafe"

	"gitlab.com/auk-go-mentoring/enum/jsontype"
	"gitlab.com/auk-go/core/issetter"
	"gitlab.com/auk-go/enum/brackets"
	"gitlab.com/auk-go/enum/dbaction"
	"gitlab.com/auk-go/enum/instructiontype"
	"gitlab.com/auk-go/enum/strtype"
)

type SomeClass struct {
	X string
	Y func() bool
}

func (receiver SomeClass) String() string {
	return "someclas alim"
}

func main() {
	// stringTypeTest()
	// bracketsTest()
	//
	// // Windwos_Version()
	// // serializeDeserializeTester()
	// err := os.Setenv("myname", "alim")
	//
	// fmt.Println(iserror.AllDefined(nil, errors.New("x")))
	//
	// fmt.Println("myname, err:", err)

	myType := jsontype.OptionC

	if myType.IsOptionA() {
		//
	}

	switch myType {
	case jsontype.Invalid:
	case jsontype.OptionB:

	}

	fmt.Println(myType)
}

func stringTypeTest() {
	fmt.Println(instructiontype.New("DependsOnx"))
	fmt.Println(dbaction.New("Create"))

	alimStrType := strtype.New("alimx")

	fmt.Println(alimStrType.SafeSubString(0, 1))
	fmt.Println(alimStrType.SafeSubString(0, alimStrType.Length()))
	fmt.Println(alimStrType.SafeSubString(-1, alimStrType.Length()+5))
	fmt.Println(alimStrType.SafeSubStringStart(2))
	fmt.Println(alimStrType.SafeSubStringEnd(2))
	fmt.Println(alimStrType.SafeSplit(2))
	fmt.Println(alimStrType.SafeSplit(alimStrType.Length() + 5))
}

func bracketsTest() {
	bracket := brackets.Parenthesis

	fmt.Println(bracket.WrapAny("something"))

	bracket2 := brackets.ParenthesisStart
	fmt.Printf("sizeof(bracket2) = %d\n", unsafe.Sizeof(bracket2))

	someTrue := true
	Val := issetter.True

	fmt.Printf("sizeof(someTrue) = %d\n", unsafe.Sizeof(&someTrue))
	fmt.Printf("sizeof(Val) = %d\n", unsafe.Sizeof(&Val))

	fmt.Println(bracket2.WrapAny("something2"))
	fmt.Println(bracket2.WrapFmtString("something to do with {wrapped}", "something2"))
	fmt.Println(bracket2.WrapSkipOnExist("(something2)"))
	fmt.Println(bracket2.IsWrapped("(something2)"))
}
